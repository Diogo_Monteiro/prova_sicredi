package br.com.sicredi.teste;

import io.qameta.allure.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class SicrediPageObject {

	private static final String URL_GROCERYCRUD = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
	private WebDriver driver;
	public  SicrediPageObject() {
		System.setProperty("webdriver.Chrome.driver", "src\\driver\\chromedriver.exe");
		ChromeDriverService service = new ChromeDriverService.Builder()

				.usingDriverExecutable(new File("src\\driver\\chromedriver.exe"))

				.usingAnyFreePort()

				.build();

		try {
			service.start();
		} catch (IOException e) {
			e.printStackTrace();
		}


		this.driver = new ChromeDriver(service);
		this.driver.navigate().to(URL_GROCERYCRUD);
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(Duration.ofMillis(8000));
	}


	@Step("Alterar o tema para o modelo V4 - Change theme for V4 template")
	public void modifyTheme() throws InterruptedException {
		WebElement btnTheme = driver.findElement(By.id("switch-version-select"));
		btnTheme.click();
		Thread.sleep(300);
		screenshot();

		WebElement optionV4Theme = driver.findElement(By.xpath("/html/body/div[1]/select/option[4]"));
		optionV4Theme.click();
		screenshot();
	}


	@Step("Botão Add Record - Button Add Record")
	public void btnAddRecord() throws InterruptedException {
		WebElement btnAddRecord = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[1]/div[1]/a"));
		btnAddRecord.click();
		Thread.sleep(300);
		screenshot();
	}

	@Step("Inserir name - Insert name")
	public void insertName(String value) throws InterruptedException {
		WebElement name = driver.findElement(By.id("field-customerName"));
		name.sendKeys(value);

		screenshot();
	}

	@Step("Inserir sobrenome - Insert last name")
	public void insertLastName(String value) throws InterruptedException {
		WebElement lastName = driver.findElement(By.id("field-contactLastName"));
		lastName.sendKeys(value);
		Thread.sleep(300);
		screenshot();
	}

	@Step("Inserir primeiro nome - Insert first name")
	public void insertFirstName(String value) throws InterruptedException {
		WebElement firstName = driver.findElement(By.id("field-contactFirstName"));
		firstName.sendKeys(value);

		screenshot();
	}

	@Step("Inserir número de telefone - Insert phone number")
	public void insertPhoneNumber(String value) throws InterruptedException {
		WebElement phone = driver.findElement(By.id("field-phone"));
		phone.sendKeys(value);

		screenshot();
	}

	@Step("Inserir 1º endereço - Insert 1º adress")
	public void insertAddressLine1(String value) throws InterruptedException {
		WebElement addressLine1 = driver.findElement(By.id("field-addressLine1"));
		addressLine1.sendKeys(value);

		screenshot();
	}

	@Step("Inserir 2º endereço - Insert 2º adress")
	public void insertAddressLine2(String value) throws InterruptedException {
		WebElement addressLine2 = driver.findElement(By.id("field-addressLine2"));
		addressLine2.sendKeys(value);

		screenshot();
	}

	@Step("Inserir nome da cidade - Insert city name")
	public void insertCity(String value) throws InterruptedException {
		WebElement city = driver.findElement(By.id("field-city"));
		city.sendKeys(value);

		screenshot();
	}

	@Step("Inserir nome do estado - Insert state name")
	public void insertState(String value) throws InterruptedException {
		WebElement state = driver.findElement(By.id("field-state"));
		state.sendKeys(value);

		screenshot();
	}

	@Step("Inserir código postal - Insert postal code")
	public void insertPostalCode(String value) throws InterruptedException {
		WebElement postalCode = driver.findElement(By.id("field-postalCode"));
		postalCode.sendKeys(value);

		screenshot();
	}

	@Step("Inserir nome do país - Insert name country")
	public void insertCountry(String value) throws InterruptedException {
		WebElement country = driver.findElement(By.id("field-country"));
		country.sendKeys(value);

		screenshot();
	}

	@Step("Inserir número do empregador - Insert employeer number")
	public void insertEmployeerNumber(String value) throws InterruptedException {
		WebElement employeer = driver.findElement(By.id("field-salesRepEmployeeNumber"));
		employeer.sendKeys(value);

		screenshot();
	}

	@Step("Inserir limite de crédito  - Insert credit limit")
	public void insertCreditLimit(String value) throws InterruptedException {
		WebElement creditLimit = driver.findElement(By.id("field-creditLimit"));
		creditLimit.sendKeys(value);


		screenshot();
	}

	@Step("Botão 'Salvar' - Button 'Save'")
	public void btnSave() throws InterruptedException {
		WebElement btnSave = driver.findElement(By.id("form-button-save"));
		btnSave.click();

		Thread.sleep(300);
		screenshot();
	}

	@Step("Validar mensagem de sucesso - Validate success message")
	public void validateSucessMessage() throws InterruptedException {
		Thread.sleep(3000);
		WebElement resultsuccessfully = driver.findElement(By.id("report-success"));
		String resultText = resultsuccessfully.getText();

		Assert.assertTrue(resultText.contains("Your data has been successfully stored into the database. Edit Record or Go back to list"));
		Thread.sleep(300);
		screenshot();
	}



	@Step("Validar mensagem de item deletado do pop-up - Validate popup deleted item message")
	public void validatePopupDeletedMessage() throws InterruptedException {
		Thread.sleep(5000);
		WebElement validatePopupMessage = driver.findElement(By.xpath("/html/body/div[3]"));
		String resultText = validatePopupMessage.getText();

		Assert.assertTrue(resultText.contains("Your data has been successfully deleted from the database."));
		Thread.sleep(300);
		screenshot();
	}

	@Step("Botão 'Voltar' - Button 'Save and go to list'")
	public void btnSaveAndGoBackToList() throws InterruptedException {

		WebElement btnSaveAndGoBackToList = driver.findElement(By.id("save-and-go-back-button"));
		btnSaveAndGoBackToList.click();

		Thread.sleep(2000);
		screenshot();
	}



	@Step("Pesquisar nome do cliente - Search customer name")
	public void searchCustomerName(String value) throws InterruptedException  {

		Thread.sleep(500);
		WebElement searchCustomerName = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[3]/input"));
		searchCustomerName.sendKeys(value);
		Thread.sleep(2000);


		screenshot();
	}

	@Step("Caixa de marcação 'Actions' - Checkbox 'Actions'")
	public void ckbxActions() throws InterruptedException {
		Thread.sleep(2000);
		WebElement ckbxActions = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/tbody/tr/td[1]/input"));
		ckbxActions.click();
		Thread.sleep(500);
		screenshot();
	}

	@Step("Botão 'Delete' - Button 'Delete'")
	public void btnDelete() throws InterruptedException {

		WebElement btnDelete = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[2]/div[1]/a"));
		btnDelete.click();
		Thread.sleep(300);
		screenshot();
	}

	@Step("Validar mensagem de item deletado - Validate deleted item message")
	public void validateDeletedItemMessage() throws InterruptedException {
		Thread.sleep(100);
		WebElement validateMessage = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[2]/p[2]"));
		String resultText = validateMessage.getText();
		Assert.assertTrue(resultText.contains("Are you sure that you want to delete this"));
		Thread.sleep(400);
		screenshot();
	}

	@Step("Botão pop-up 'Delete' - Button pop-up 'Delete'")
	public void btnPopUpDelete() throws InterruptedException {
		Thread.sleep(400);
		WebElement btnPopUpDelete = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[3]/div/div/div[3]/button[2]"));
		btnPopUpDelete.click();
		Thread.sleep(600);

		screenshot();
	}

	@Step("Botão 'Atualizar' - Button 'Refresh'")
	public void btnRefresh()  {

		WebElement btnEdit = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[2]/table/thead/tr[2]/td[2]/div[2]/a"));
		btnEdit.click();

		screenshot();
	}


	@Step("Fechar navegador - Close browser")
	public void close(){
		this.driver.quit();
	}

	@Attachment(value = "Passos", type = "image/png")
	public byte[] screenshot() {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}
}
