import br.com.sicredi.teste.SicrediPageObject;

import br.com.sicredit.teste.ModelSicredi;
import br.com.sicredit.teste.ModelSicrediModify;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SicrediPageTeste{

    private SicrediPageObject pageSicredi;

    @BeforeTest
    public void beforeTest(){ this.pageSicredi = new SicrediPageObject();}

    @AfterTest
    public void afterTest(){this.pageSicredi.close();}

    @Test(description = "Teste Sicredi 1")
    public void testeSicredi1() throws InterruptedException {

        ArrayList<ModelSicredi> models = new ArrayList<ModelSicredi>();
        ModelSicredi modelo = new ModelSicredi();
        modelo.setName("Teste Sicredi");
        modelo.setLastName("Teste");
        modelo.setFirstName("Diogo Monteiro");
        modelo.setPhoneNumber("51 9999-9999");
        modelo.setAddressLine1("Av Assis Brasil, 3970");
        modelo.setAddressLine2("Torre D");
        modelo.setCity("Porto Alegre");
        modelo.setState("RS");
        modelo.setPostalCode("91000-000");
        modelo.setCountry("Brasil");
        modelo.setEmployeerNumber("01.181.521/0001-55");
        modelo.setCreditLimit("200");
        modelo.setSearchCustomerName("Teste Sicredi");



        models.add(modelo);

        for (ModelSicredi model: models) {
            pageSicredi.modifyTheme();
            pageSicredi.btnAddRecord();
            pageSicredi.insertName(model.getName());
            pageSicredi.insertLastName(model.getLastName());
            pageSicredi.insertFirstName(model.getFirstName());
            pageSicredi.insertPhoneNumber(model.getPhoneNumber());
            pageSicredi.insertAddressLine1(model.getAddressLine1());
            pageSicredi.insertAddressLine2(model.getAddressLine2());
            pageSicredi.insertCity(model.getCity());
            pageSicredi.insertState(model.getState());
            pageSicredi.insertPostalCode(model.getPostalCode());
            pageSicredi.insertCountry(model.getCountry());
            pageSicredi.insertEmployeerNumber(model.getEmployeerNumber());
            pageSicredi.insertCreditLimit(model.getCreditLimit());
            pageSicredi.btnSave();
            pageSicredi.validateSucessMessage();
        }

    }

    @Test(dependsOnMethods={"testeSicredi1"}, description = "Teste Sicredi 2")
    public void testeSicredi2() throws InterruptedException {


        ArrayList<ModelSicrediModify> models = new ArrayList<ModelSicrediModify>();
        ModelSicrediModify modelo = new ModelSicrediModify();

        modelo.setSearchCustomerName("Teste Sicredi");


        models.add(modelo);


        for (ModelSicrediModify model: models) {

            pageSicredi.btnSaveAndGoBackToList();
            pageSicredi.searchCustomerName(model.getSearchCustomerName());
            pageSicredi.btnRefresh();
            pageSicredi.ckbxActions();
            pageSicredi.btnDelete();
            pageSicredi.validateDeletedItemMessage();
            pageSicredi.btnPopUpDelete();
            pageSicredi.validatePopupDeletedMessage();

        }

    }



}
